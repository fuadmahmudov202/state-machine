package az.ingress.statemachine.domain;

import az.ingress.statemachine.user.UserStatus;
import jakarta.persistence.*;
import lombok.Data;

@Entity
@Data
public class Users {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String firstname;
    private String lastname;
    private String username;
    private Integer age;
    private UserStatus status;
    private String password;
}
