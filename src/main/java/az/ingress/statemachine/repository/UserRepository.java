package az.ingress.statemachine.repository;

import az.ingress.statemachine.domain.Users;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<Users,Long> {
}
