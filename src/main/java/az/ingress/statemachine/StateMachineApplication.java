package az.ingress.statemachine;

import az.ingress.statemachine.domain.Users;
import az.ingress.statemachine.repository.UserRepository;
import az.ingress.statemachine.user.TransitionService;
import az.ingress.statemachine.user.UserStatus;
import az.ingress.statemachine.user.transition.*;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@RequiredArgsConstructor
public class StateMachineApplication implements CommandLineRunner {

    private final TransitionService transitionService;
    private final UserRepository userRepository;
    public static void main(String[] args) {
        SpringApplication.run(StateMachineApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        Users users= new Users();
        users.setAge(20);
        users.setFirstname("test");
        users.setLastname("test");
        users.setUsername("test");
        users.setPassword("318374gu");
        users.setStatus(UserStatus.DRAFT);
        userRepository.save(users);
        transitionService.transition(users.getId(), Submit.NAME);
        transitionService.transition(users.getId(), Reject.NAME);
        transitionService.transition(users.getId(), Submit.NAME);
        transitionService.transition(users.getId(), Approve.NAME);
        transitionService.transition(users.getId(), Approve.NAME);
        transitionService.transition(users.getId(), Approve.NAME);
        transitionService.transition(users.getId(), Notify.NAME);

        transitionService.transition(users.getId(), Close.NAME);


    }
}
