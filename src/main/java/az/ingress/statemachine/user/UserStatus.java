package az.ingress.statemachine.user;

import az.ingress.statemachine.user.transition.*;
import lombok.Getter;

import java.util.Arrays;
import java.util.List;

public enum UserStatus {
    DRAFT(Submit.NAME),
    IN_REVIEW(Approve.NAME, Reject.NAME),
    APPROVED(Approve.NAME, Notify.NAME),
    NOTIFIED(Close.NAME),
    CLOSED();
    private final List<String> transitions;

    UserStatus(String... transitions) {
        this.transitions = Arrays.asList(transitions);
    }

    public List<String> getTransitions() {
        return transitions;
    }
}
