package az.ingress.statemachine.user;

import az.ingress.statemachine.domain.Users;
import az.ingress.statemachine.dto.UserDto;
import az.ingress.statemachine.repository.UserRepository;
import az.ingress.statemachine.user.events.UserTransitionedEvent;
import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class TransitionServiceImpl implements TransitionService<UserDto> {
    private final UserRepository userRepository;
    private final ModelMapper modelMapper;
    private final ApplicationEventPublisher applicationEventPublisher;
    private final List<Transition> transitionList;
    private Map<String, Transition> transitionMap;

    @Override
    public UserDto transition(Long id, String transitionStr) {
        final Transition transition = Optional.ofNullable(transitionMap.get(transitionStr))
                .orElseThrow(() -> new RuntimeException("Unknown transitionStr :" + transitionStr));
        return userRepository.findById(id)
                .map(users -> {
                    checkAllowed(transitionStr, users.getStatus());
                    transition.applyProcessing(modelMapper.map(users,UserDto.class));
                    return updateStatus(users,transition.getTargetStatus());
                })
                .map(user -> modelMapper.map(user, UserDto.class))
                .orElseThrow(() -> new IllegalArgumentException("No user Found with id :" + id));
    }

    private Users updateStatus(Users users, UserStatus targetStatus) {
        UserStatus existStatus=users.getStatus();
        users.setStatus(targetStatus);
        Users updated=userRepository.save(users);
        var event= new UserTransitionedEvent(this,existStatus,modelMapper.map(updated,UserDto.class));
        applicationEventPublisher.publishEvent(event);
        return users;
    }


    @Override
    public List<String> getAllowedTransitions(Long id) {
        return userRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("user not found with id " + id))
                .getStatus().getTransitions();
    }

    @PostConstruct
    private void initTransactions() {
        Map<String, Transition> transitionHashMap = new HashMap<>();
        for (Transition userTransition : transitionList) {
            if (transitionHashMap.containsKey(userTransition.getName()))
                throw new IllegalStateException("Duplicate transition :" + userTransition.getName());
            transitionHashMap.put(userTransition.getName(), userTransition);
        }
        this.transitionMap = transitionHashMap;
    }

    private void checkAllowed(String transition, UserStatus status) {
        status.getTransitions().stream()
                .filter(transition::equals)
                .findFirst()
                .orElseThrow(() -> new RuntimeException("Transition :" + transition + "from status: " + status + "is not allowed"));
    }
}
