package az.ingress.statemachine.user.transition;

import az.ingress.statemachine.dto.UserDto;
import az.ingress.statemachine.user.Transition;
import az.ingress.statemachine.user.UserStatus;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class Notify implements Transition<UserDto> {
    public static final String NAME = "notified";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public UserStatus getTargetStatus() {
        return UserStatus.NOTIFIED;
    }

    @Override
    public void applyProcessing(UserDto user) {
        log.info("Users with id {} is transitioning to {} state",user.getId(),getTargetStatus());
    }
}
