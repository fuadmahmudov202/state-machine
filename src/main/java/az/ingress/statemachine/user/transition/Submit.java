package az.ingress.statemachine.user.transition;

import az.ingress.statemachine.dto.UserDto;
import az.ingress.statemachine.user.Transition;
import az.ingress.statemachine.user.UserStatus;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class Submit implements Transition<UserDto> {
    public static final String NAME = "submit";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public UserStatus getTargetStatus() {
        return UserStatus.IN_REVIEW;
    }

    @Override
    public void applyProcessing(UserDto user) {
        //DO Validation
        if (user.getAge() < 18)
            throw new RuntimeException("age is less than 18");
        log.info("Users with id {} is transitioning to {} state",user.getId(),getTargetStatus());

    }
}
