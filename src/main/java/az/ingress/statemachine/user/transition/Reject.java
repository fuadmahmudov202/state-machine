package az.ingress.statemachine.user.transition;

import az.ingress.statemachine.dto.UserDto;
import az.ingress.statemachine.user.Transition;
import az.ingress.statemachine.user.UserStatus;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
@Slf4j
@Component
public class Reject implements Transition<UserDto> {
    public static final String NAME = "reject";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public UserStatus getTargetStatus() {
        return UserStatus.DRAFT;
    }

    @Override
    public void applyProcessing(UserDto user) {
        //DO Validation
        log.info("Users with id {} is transitioning to {} state",user.getId(),getTargetStatus());
    }
}
