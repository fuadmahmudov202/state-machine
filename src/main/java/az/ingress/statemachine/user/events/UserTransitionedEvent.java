package az.ingress.statemachine.user.events;

import az.ingress.statemachine.dto.UserDto;
import az.ingress.statemachine.user.UserStatus;
import lombok.Getter;
import org.springframework.context.ApplicationEvent;
@Getter
public class UserTransitionedEvent extends ApplicationEvent {
    private final UserDto userDto;
    private final UserStatus userStatus;
    public UserTransitionedEvent(Object source, UserStatus existStatus, UserDto dto) {
        super(source);
        this.userDto=dto;
        this.userStatus=existStatus;
    }
}
